const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({

	name : {
		type : String,
		required : [true, "name is required"]
	},
	description : {
		type : String,
		required : [true]
	},
	price : {
		type : Number
	},
	isAdmin : {
		type : Boolean,
		default : true
	},
	isActive : {
		type : Boolean,
	default : true
	},
	createdOn : {
		type : Date,
	default : new Date()
	},
	userOrders : [{
		userId : {
			type : String,
			required : [true, "user ID is required"] 
		}
	}]


})

module.exports = mongoose.model("Product", productSchema);


