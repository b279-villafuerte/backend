const Product = require("../models/Product");

// Create a new product
module.exports.addProduct = (data) => {
	console.log(data.isAdmin);
	if(data.isAdmin) {
		let newProduct = new Product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		})

		return newProduct.save().then((product, error) => {
			if(error){
				return error;
			}
			return product;
		})
	} 

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
};


// retrieve all product function (admin)
module.exports.getAllProduct = (reqBody,isAdmin) => {
	if(isAdmin){
		return Product.find({}).then(result => {
			return result;
		})
	}
		let message = Promise.resolve ("Only admin can access");
		return message.then((value) => {
			return value;
		})
	
};


// retrieve all ACTIVE products function
module.exports.getAllActive = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	})
};

// Retreiving a specific product

module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result =>{
		return result;
	})
};

// Update a product
module.exports.updateProduct = (reqParams, reqBody, isAdmin) => {
	console.log(isAdmin);

	if(isAdmin){
		let updatedProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
		}

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if(error){
				return false;
			}else{
				return "Product has been Updated!";
			}
		})
	}

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("Only admin can access");

	return message.then((value) => {
		return value
	})
	
};

// Archive a course
module.exports.archiveProduct = (reqParams, isAdmin) => {
	console.log(isAdmin);
	let updateActiveField = {
		isActive : false
	};

	if(isAdmin){
		return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((course, error) => {

			// If the product is not archived
			if (error) {
				return false;
			// If the product is archived successfully
			} else {
				return "The item has been archived";
			}
		});	
	}

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("Only admin can access");

	return message.then((value) => {
		return value
	})
};

// Creating an order (non-admin)

module.exports.orderProduct = (data) => {
	
}


