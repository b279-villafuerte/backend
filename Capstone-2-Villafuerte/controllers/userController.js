// The "User" variable is defined using a capitalized letter to indicate that what we are using is the "User" model for code readability
const User = require("../models/Users");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product");

// check if email exists
module.exports.checkEmailExists = (reqBody) => {

	// The result is sent back to the frontend via the "then" method found in the route file
	return User.find({email : reqBody.email}).then(result => {

		// The "find" method returns a record if a match is found
		if (result.length > 0) {

			return "email already exists";

		// No duplicate email found
		// The user is not yet registered in the database
		} else {

			return "email available";

		}
	})

};

// User registration
module.exports.registerUser = (reqBody) => {

	let newUser = new User({

		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)

	})

	return newUser.save().then((user, error) => {

		// User registration failed
		if (error) {

			return false;

		// User registration successful

		} else {

			return "you are now registered";

		}

	})

};

// User log in
module.exports.loginUser = (reqBody) => {
	
	return User.findOne({email : reqBody.email}).then(result => {

		// User does not exist
		if(result == null){

			return "User does not exist";

		// User exists
		} else {

			
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			
			if (isPasswordCorrect) {
				return { access : auth.createAccessToken(result) }

			// Passwords do not match
			} else {

				return "Password incorrect";

			};

		};

	});

};

// user get profile
module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});

};

// create order (non-admin)

module.exports.createOrder = async (data) => {
    if(!data.isAdmin){
        let productTemp = [];
        let totalAmountTemp = 0;
        let isProductUpdated;


        data.products.forEach(singleProduct => {
            console.log(singleProduct);
            Product.findById(singleProduct.productId).then(product => { 
                productTemp.push(
                    {
                        productId : singleProduct.productId,
                        productName : product.name,
                        quantity : singleProduct.quantity
                    }
                )
                totalAmountTemp = totalAmountTemp + (product.price * singleProduct.quantity)
            }
            )
            isProductUpdated = Product.findById(singleProduct.productId).then(product =>{

                product.userOrders.push({userId : data.userId})
                return product.save().then((product, error) => {
                    if(error){
                        return false;
                    } else {
                        return true;
                    }
                })
            })
        });


        let isUserUpdated = await User.findById(data.userId).then(user => {
                user.orderedProduct.push(
                    {

                        product : productTemp,
                        totalAmount : totalAmountTemp
                    }
                );
                return user.save().then((user, error) => {
                    if(error){
                        return false;
                    }else{
                        return true;
                    }
                })
        })


        if(isUserUpdated && isProductUpdated){
            return true;
        }else{
            return "Something went wrong with your request. Please try again later!";
        }
    } else {
        return Promise.resolve("Admins can't checkout products")
    }
};


