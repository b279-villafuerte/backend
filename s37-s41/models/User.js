// ACTIVITY S37-S41
const mongoose = require("mongoose");
const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		required : [true, "NAME IS REQUIRED"]
	},
	lastName : {
		type : String,
		required : [true, "LAST NAME IS REQUIRED"]
	},
	email : {
		type : String,
		required : [true, "EMAIL IS REQUIRED"]
	},
	password : {
		type : String,
		required : [true, "PASSWORD IS REQUIRED"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	mobileNo : {
		type : String,
		required : true
	},
	enrollments : [{
		courseId : {
			type : String,
			required : [true, "COURSE ID IS REQUIRED"]
		},
		enrolledOn : {
			type : Date,
			default : new Date()
		},
		status : {
			type : String,
			default : true
		}
	}]

});

module.exports = mongoose.model("User", userSchema);