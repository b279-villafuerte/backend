const mongoose = require("mongoose");
const courseSchema = new mongoose.Schema({
	name : {
		type : String,
		required : [true, "COURSE NAME IS REQUIRED"]
	},
	description : {
		type : String,
		required : [true, "COURSE DESCRIPTION IS REQUIRED"]
	},
	price : {
		type : Number,
		required : [true, "COURSE PRICE IS REQUIRED"]
	},
	isActive : {
		type : Boolean,
		default : true
	},
	createdOn : {
		type : Date,
		// The "new Date()" expression instantiates current date
		default : new Date()
	},
	enrollees : [
			{
				userId : {
					type : String,
					required : [true, "USER ID IS REQUIRED"]
				},
				enrolledOn : {
					type : Date,
					default : new Date() 
				}
			}
		]
});

module.exports = mongoose.model("Course", courseSchema);