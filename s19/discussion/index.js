// console.log("hello world");

// [SECTION] if, else, else if Statement
// conditional statements
	// it allows us to control the flow of out program
	// it allows us to run a statement or instruction if the condition is met


let numA = -1;

// if statement
// executes a statement if a specified condition is "true"
/*

SYNTAX

if (condition){
	/codeblock or statement
}

*/
if (numA < 0){
	console.log("hello: ");
}

console.log(numA < 0);

// update numA 
numA = 0

if (numA < 0){
	console.log("Hello again id numA is 0")
}

// Another example 

let city = "New York";

if (city === "New York"){
	console.log("Welcome to New York")
}

// else if clause

/* 
    - Executes a statement if previous conditions are false and if the specified condition is true
    - The "else if" clause is optional and can be added to capture additional conditions to change the flow of a program

*/

let numH = 1;

if (numA < 0){
	console.log("hello");
} else if (numH > 0){
	console.log("WORLD!")
}

numA = 1;

if (numA > 0){
	console.log("hello");
} else if (numH > 0){
	console.log("WORLD!")
}

// else statement

/* 
    - Executes a statement if all other conditions are false
    - The "else" statement is optional and can be added to capture any other result to change the flow of a program
*/

if (numA > 0){
	console.log("Hello")
} else if (numH == 0){
	console.log("World")
} else {
	console.log("Again")
}


// else{
// 	console.log("test")
// }

// else if (numH === 0){
// 	console.log("world")
// } else{
// 	console.log("again")
// }

/*STRUCTURE
if
if, else if
if, else
avoid using this structure -> error
if
else
else if*/


// if, else if, else statements with Functions

let message = "No message";
console.log(message);

function determineTyphoonIntensity(windSpeed){
	if (windSpeed < 30){
		return "Not a typhoon yet.";
	} else if (windSpeed <= 61){
		return "Tropcial Depression Detected!";
	} else if (windSpeed >=62 && windSpeed <= 88){
		return "Tropical Storm Detected!";
	} else if (windSpeed >= 89 && windSpeed <= 117){
		return "Severe tropical strom Detected"
	} else{
		return "Typhoon Detected."
	}



}

message = determineTyphoonIntensity();
console.log(message);

// if (message == "Typhoon Detected.") {
// 	console.error(message);
// }

// // Truthy and Falsy


// // TRUTHY
// if (true) {
// 	console.log("truthy");
// }

// if (1) {
// 	console.log("truthy")
// }
// if ([]) {
// 	console.log("truthy")
// }

// // FALSY
// if (false) {
// 	console.log("falsy");
// }

// if (0) {
// 	console.log("falsy");
// }

// if (undefined) {
// 	console.log("falsy");
// }

// // [SECTION] Conditional (Ternary) Operator

// /* 
//     - The Conditional (Ternary) Operator takes in three operands:
//         1. condition
//         2. expression to execute if the condition is truthy
//         3. expression to execute if the condition is falsy
//     - Can be used as an alternative to an "if else" statement
//     - Ternary operators have an implicit "return" statement meaning that without the "return" keyword, the resulting expressions can be stored in a variable
// - Commonly used for single statement execution where the result consists of only one line of code
//     - For multiple lines of code/code blocks, a function may be defined then used in a ternary operator
//     - Syntax
//         (expression) ? ifTrue : ifFalse;
// */

// // Single statement execution

// let ternaryResult = (1 > 18) ? true : false;
// console.log("Result of ternary operator: " + ternaryResult);


// // Multiple Statement Execution

// let name;

// function isOfLegalAge(){
// 	name = "John";
// 	return "You are of the legal age limit"
// }

// function isUnderAge(){
// 	name = "Jane";
// 	return "You are under the age limit"
// }

// let age = parseInt(prompt("What is your age?: "));

// console.log(age);

// let legalAge = (age >= 18) ? isOfLegalAge() : isUnderAge();
// console.log("result of ternary operator in function: " + legalAge + ", " + name);

// // [SECTION] Switch Statement

// /*
// SYNTAX

// switch(expression){
// 	case value:
// 		statement;
// 		break;
// 	default:
// 		statement;
// 		break;
// }

// */

// let day = prompt("What day of the week is it today?").toLowerCase();
// console.log(day);

// switch(day) {
// 	case "monday":
// 		console.log("The color of the day is red");
// 		break;
// 	case "tuesday":
// 		console.log("The color of the day is orange");
// 		break;
// 	case "wednesday":
// 		console.log("The color of the day is yellow");
// 		break;
// 	case "thursday":
// 		console.log("The color of the day is green");
// 		break;
// 	case "friday":
// 		console.log("The color of the day is blue");
// 		break;
// 	case "saturday":
// 		console.log("The color of the day is indigo");
// 		break;
// 	case "sunday":
// 		console.log("The color of the day is violet");
// 		break;
// 	default:
// 		console.log("Please input a valid day");
// 		break;

// }

// // [SECTION] Try-catch-finally

// function showIntensityAlert(windSpeed){
// 	try{
// 		alerat(determineTyphoonIntensity(windSpeed));
// 	}catch (error){
// 		console.log(typeof error);
// 		console.warn(error.message);
// 	}finally{
// 		// continue the execution of the code
// 		alert("Intensity update will show new alert")
// 	}
// }

// showIntensityAlert(56);